$(document).ready(function() {
    $('.filter-btn').click(function(){
        if ($(this).hasClass('active')) {
            return;
        }
        $('.filter-btn.active').button('toggle');
        $(this).button('toggle');
        if ($(this).text() === 'All') {
            $('.card').show(300);
        } else {
            $('.card').hide().filter('[data-key="' + $(this).text().toLowerCase() + '"]').show(300)
        }
    })
})