$(document).ready(function() {
    $('.card').click(function() {
        $('body').toggleClass('stop-scroll');
        let image = $(this).find('.card-img').attr('src');
        let author = $(this).find('bottom-block').html();
        let modal = $('#modal');
        modal.css('display', 'flex');
        modal.find('.image').attr('src', image);
        modal.find('.download-btn').attr('href', image);
        modal.find('.bottom-block').html(author);
    });
    $('#modal .close').click(function(){
        $('#modal').css('display', 'none');
        $('body').toggleClass('stop-scroll');
    });
})